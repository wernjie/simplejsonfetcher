//
//  SimpleJSONFetcher.swift
//
//  Created by Lim Wern Jie.
//  Copyright © 2019 Lim Wern Jie. All rights reserved.
//

import Foundation

/**
 Simple class for easily fetching JSON data from a URL.
 */
class SimpleJSONFetcher {
    /**
     Fetches JSON data from a url.
    
     - Parameters:
        - url: source url.
        - response: returns a tuple containing the JSON response and any errors.
                    
     JSON response is provided in the form of Swift arrays or dictionarys, so `{"value": [1,2], "id": 1}` will be passed through as a dictionary of type `[String: Any]`, which the key `"value"` returns an object of type `[Int]` while the key `"id"` returns an `Int`.
     */
    class func getFrom(_ url: URL, response: @escaping (Any?, Error?) -> ()) {
        DispatchQueue.global().async {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) -> Void in
                if let data = data {
                    do {
                        let response1 = try JSONSerialization.jsonObject(with: data, options: [])
                        LOG("JSON response acquired.")
                        DispatchQueue.main.async {
                            LOG("Returning JSON response.")
                            response(response1, nil)
                        }
                    } catch let e {
                        LOG("Response acquired, but it isn't JSON formatted. Error: \(e.localizedDescription)")
                        response(nil, e)
                    }
                } else {
                    LOG("No response found. Error: \(error?.localizedDescription ?? "no error")")
                    response(nil, error)
                }
            })
            task.resume()
        }
    }
}

fileprivate func LOG(_ str: String) {
    //Comment out the below line to disable logging.
    print("[SimpleJSONFetcher]: " + str)
}
